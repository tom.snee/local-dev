#!/usr/bin/env sh

awk -v nlines_after=4 '/- name: KAFKA_(USERNAME|PASSWORD)/ {for (i=0; i<nlines_after; i++) {getline}; next} 1'
