#!/usr/bin/env sh

helm uninstall core-zookeeper
kubectl delete pvc data-core-zookeeper-0
