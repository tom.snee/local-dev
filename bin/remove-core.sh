#!/usr/bin/env sh

helm uninstall core-ui
helm uninstall core-order-allocation-notifications
helm uninstall core-order-allocation-service
helm uninstall core-deal-announcement-service
helm uninstall core-fix-service
helm uninstall core-authentication-service
helm uninstall local-secrets
helm uninstall core-audits-db-migrations
helm uninstall core-auth-db-migrations
helm uninstall core-fix-db-migrations
helm uninstall core-fix-seed-db-migrations
helm uninstall core-orders-db-migrations
helm uninstall core-tranches-db-migrations
helm uninstall core-tranches-seed-db-migrations
helm uninstall regcred
kubectl delete -f gateway.yaml
kubectl delete job core-audits-db-migrations-flyway-chart core-auth-db-migrations-flyway-chart \
  core-fix-db-migrations-flyway-chart core-fix-seed-db-migrations-flyway-chart \
  core-orders-db-migrations-flyway-chart core-tranches-db-migrations-flyway-chart \
  core-tranches-seed-db-migrations-flyway-chart
