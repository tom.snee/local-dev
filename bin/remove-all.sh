#!/usr/bin/env sh

BINDIR=$(dirname "$0")

${BINDIR}/remove-core.sh
${BINDIR}/remove-postgres.sh
${BINDIR}/remove-kafka.sh
${BINDIR}/remove-zookeeper.sh

echo "Not removing external-secrets Helm installation."
echo "Not removing Istio."
