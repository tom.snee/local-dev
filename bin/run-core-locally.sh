#!/usr/bin/env bash

set -e

BINDIR=$(dirname "$0")
ENVIRONMENT=local
ISTIO_VERSION=1.21.2

APPLICATION_CONFIG=${1:-"${BINDIR}/../../application-config"}
if [ ! -d "${APPLICATION_CONFIG}/${ENVIRONMENT}" ]; then
  echo "The application-config repo with environment $ENVIRONMENT has not been cloned to $APPLICATION_CONFIG."
  echo "If it is somewhere else, pass its location as the first parameter to this script."
  exit 1
fi

if [ ! -f "${BINDIR}/../secrets.yaml" ]; then
  echo "You must rename emptysecrets.yaml to secrets.yaml and fill it with valid values."
  exit 1
fi

if [ -z "$AWS_ACCESS_KEY_ID" -o -z "$AWS_SECRET_ACCESS_KEY" -o -z "$AWS_SESSION_TOKEN" ]; then
  echo "All of AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_SESSION_TOKEN must be set."
  echo "See https://d-90679fc682.awsapps.com/start/#/?tab=accounts"
  exit 2
elif ! docker run --rm -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN amazon/aws-cli --region us-east-1 sts get-caller-identity; then
  echo "See https://d-90679fc682.awsapps.com/start/#/?tab=accounts"
  exit 3
else
  dockerPassword=$(docker run --rm -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY -e AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN amazon/aws-cli --region us-east-1 ecr get-login-password)
  if [ -z "$dockerPassword" ]; then
    echo "Cannot pull images from ECR, exiting."
    exit 4
  fi
fi

if ! type -p kubectl > /dev/null; then
  echo "Install Docker Desktop."
  exit 5
fi

echo "Changing Kubernetes context to docker-desktop."
if kubectl config use-context docker-desktop; then
  kubectl label node docker-desktop app-namespace=drb-core
else
  echo "You do not appear to be using docker-desktop. Make sure you have the correct K8s context set!"
  echo "You must manually label at least one node in your cluster with app-namespace=drb-core."
fi

if ! type -p helm > /dev/null; then
  echo "Installing Helm. sudo may prompt you for your password."
  sudo apt-get install helm
fi

if ! type -p yq > /dev/null; then
  echo "Installing yq. sudo may prompt you for your password."
  sudo apt-get install yq
fi

ISTIOCTL=$(type -p istioctl || true)
if [ -z "$ISTIOCTL" ]; then
  if [ ! -d "istio-$ISTIO_VERSION" ]; then
    echo "Cannot find istioctl, installing version $ISTIO_VERSION in $PWD."
    curl -L https://istio.io/downloadIstio | ISTIO_VERSION=$ISTIO_VERSION sh -
  fi
  ISTIOCTL=./istio-${ISTIO_VERSION}/bin/istioctl
fi
if ! $ISTIOCTL verify-install; then
  $ISTIOCTL install --set profile=default -y
fi
kubectl label namespace default istio-injection=enabled

helm upgrade --install --atomic --set name=regcred --set "username=AWS" --set "password=$dockerPassword"  \
  --set "server=729132158013.dkr.ecr.us-east-1.amazonaws.com" regcred \
  --repo https://snowplow-devops.github.io/helm-charts/ dockerconfigjson

if ! helm status external-secrets; then
  helm install --wait --atomic external-secrets --repo https://charts.external-secrets.io/ external-secrets
fi

helm upgrade --install --atomic -f "${BINDIR}/../secrets.yaml" --set "aws.accessKeyId=$AWS_ACCESS_KEY_ID" --set "aws.secretAccessKey=$SECRET_ACCESS_KEY" local-secrets helm/local-secrets

if ! helm status core-zookeeper; then
  helm install --atomic core-zookeeper  --version 13.2.0 oci://registry-1.docker.io/bitnamicharts/zookeeper
fi

if ! helm status core-kafka; then
  helm install --wait --atomic --values secrets.yaml --set listeners.client.protocol=PLAINTEXT core-kafka --version 28.0.4 oci://registry-1.docker.io/bitnamicharts/kafka
fi

${BINDIR}/configure-kafka.sh

if ! helm status core-postgresql; then
  helm install --wait --atomic --values secrets.yaml core-postgresql --version 15.2.5 oci://registry-1.docker.io/bitnamicharts/postgresql
fi

INSTALL_CORE_FLYWAY_CHART () {
  SCHEMA="$1"
  SCHEMA_SLUG=$(echo $SCHEMA | sed s/_/-/g)
  RELEASE="${SCHEMA_SLUG}-db-migrations"
  APP_NAME=flyway
  CHART_VERSION="$2"
  if [ -z "$CHART_VERSION" ]; then
    CHART_VERSION=$(yq eval .helm-version-$APP_NAME ${APPLICATION_CONFIG}/${ENVIRONMENT}/shared/metadata.yml)
  fi
  if ! helm status "$RELEASE"; then
    helm install --atomic -f "${APPLICATION_CONFIG}/common/shared/values.yaml" \
      -f "${APPLICATION_CONFIG}/${ENVIRONMENT}/shared/values.yaml" \
      -f "${APPLICATION_CONFIG}/common/${APP_NAME}/values.yaml" \
      -f "${APPLICATION_CONFIG}/${ENVIRONMENT}/${APP_NAME}/values.yaml" \
      -f "${APPLICATION_CONFIG}/${ENVIRONMENT}/${APP_NAME}/${SCHEMA}.yaml" \
      "$RELEASE" --version "$CHART_VERSION" oci://directbooks2.jfrog.io/drb-local-helm/flyway-chart
  fi
}

INSTALL_CORE_SERVICE_CHART () {
  APP_NAME="$1"
  CHART_VERSION="$2"
  if [ -z "$CHART_VERSION" ]; then
    CHART_VERSION=$(yq eval .helm-version-$APP_NAME ${APPLICATION_CONFIG}/${ENVIRONMENT}/shared/metadata.yml)
  fi
  if ! helm status "$APP_NAME"; then
    helm install --atomic -f "${APPLICATION_CONFIG}/common/shared/values.yaml" \
      -f "${APPLICATION_CONFIG}/${ENVIRONMENT}/shared/values.yaml" \
      -f "${APPLICATION_CONFIG}/common/${APP_NAME}/values.yaml" \
      -f "${APPLICATION_CONFIG}/${ENVIRONMENT}/${APP_NAME}/values.yaml" \
      "--post-renderer=${BINDIR}/post-renderer.sh" "$APP_NAME" \
      --version "$CHART_VERSION" "oci://directbooks2.jfrog.io/drb-local-helm/${APP_NAME}-chart"
  fi
}

INSTALL_CORE_FLYWAY_CHART "core_audits"

INSTALL_CORE_FLYWAY_CHART "core_tranches"
INSTALL_CORE_FLYWAY_CHART "core_tranches_seed"
INSTALL_CORE_SERVICE_CHART "core-deal-announcement-service"

INSTALL_CORE_FLYWAY_CHART "core_auth"
INSTALL_CORE_SERVICE_CHART "core-authentication-service"

INSTALL_CORE_FLYWAY_CHART "core_fix"
INSTALL_CORE_FLYWAY_CHART "core_fix_seed"
INSTALL_CORE_SERVICE_CHART "core-fix-service"

INSTALL_CORE_FLYWAY_CHART "core_orders"
INSTALL_CORE_SERVICE_CHART "core-order-allocation-service"
INSTALL_CORE_SERVICE_CHART "core-order-allocation-notifications"

INSTALL_CORE_SERVICE_CHART "core-ui"

if [ ! -f tls/ingressgateway.crt ]; then
  mkdir tls
  openssl req -x509 -sha256 -nodes -days 365 -newkey rsa:2048 -subj '/O=DirectBooks/CN=directbooks.com' -keyout tls/root.key -out tls/root.crt
  openssl req -out tls/ingressgateway.csr -newkey rsa:2048 -nodes -keyout tls/ingressgateway.key -subj "/CN=dev.directbooks.com/O=DirectBooks"
  openssl x509 -req -sha256 -days 365 -CA tls/root.crt -CAkey tls/root.key -set_serial 0 -in tls/ingressgateway.csr -out tls/ingressgateway.crt
  kubectl delete -n istio-system secret ingressgateway || true
fi
kubectl get -n istio-system secret ingressgateway || kubectl create -n istio-system secret tls ingressgateway --key=tls/ingressgateway.key --cert=tls/ingressgateway.crt

kubectl apply -f gateway.yaml

open https://dev.directbooks.com/app/signin
