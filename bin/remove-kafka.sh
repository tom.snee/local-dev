#!/usr/bin/env sh

helm uninstall core-kafka
kubectl delete pvc data-core-kafka-controller-0 data-core-kafka-controller-1 data-core-kafka-controller-2
