#!/usr/bin/env sh

K8S_KAFKA_POD=core-kafka-controller-0
BOOTSTRAP_SERVER=localhost:9092
NUM_PARTITIONS=1
REPLICATION_FACTOR=1

for topic in drb.core.outgoing_deals drb.core.incoming_deals drb.core.incoming_orders drb.core.outgoing_deal_update drb.core.outgoing_orders drb.core.outgoing_gui_notifications drb.core.outgoing_fix drb.core.incoming_fix drb.core.outgoing_acknowledgements; do
  kubectl exec "$K8S_KAFKA_POD" -- kafka-topics.sh --bootstrap-server "$BOOTSTRAP_SERVER" --describe --topic "$topic" > /dev/null 2>&1 || kubectl exec "$K8S_KAFKA_POD" -- kafka-topics.sh --bootstrap-server "$BOOTSTRAP_SERVER" --create --topic "$topic" --partitions "$NUM_PARTITIONS" --replication-factor "$REPLICATION_FACTOR"
done
