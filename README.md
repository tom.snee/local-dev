# Local Dev

## Quick Start

### From Windows, one time only

1. Install [WSL](https://learn.microsoft.com/en-us/windows/wsl/install) and the Ubuntu Linux distribution.
2. Install [Docker Desktop](https://www.docker.com/products/docker-desktop/). Under `Settings` / `Kubernetes`,
   click "Enable Kubernetes".

### From a Linux shell, one time only

1. Install helm and yq. (The `run-core-locally.sh` script will use `sudo apt-get` to install them if
   it cannot find them.)
1. Install Istio. (If `istioctl` is not found, the script will install it in the current directory.)
1. Clone the [application-config repository](https://gitlab.com/directbook1/SysOps/application-config) and
   check out the `develop` branch. This can be done with a single command, like so:
   `git clone -b develop git@gitlab.com:directbook1/SysOps/application-config.git`.
1. Clone this repository.
1. Add valid secrets to `emptysecrets.yaml` and rename it to `secrets.yaml`. The secrets you need are:
  a. AG Grid Enterprise key. This can be found in Keeper under Cloud Engineer Share/AG Grid AWS Secret
     (not the entire JSON object, but the value of "license" within the object).
  b. directbooks-test.okta.com credentials, which can also be found in the AWS Secrets Manager under
     `/dev/core/authentication/app`.
  c. PostgreSQL credentials that are only created and used locally, so which can be any value you choose.
  d. Core services shared secrets that are only created and used locally, so which can be any value you choose.

### From a Linux shell, every time
1. Quit all non-essential WSL applications. The standard DirectBooks developer PC does not allocate enough RAM
   to WSL to run all the Core services as well as an IDE and web browser at the same time.
1. Set environment variables AWS\_ACCESS\_KEY\_ID, AWS\_SECRET\_ACCESS\_KEY, and AWS\_SESSION\_TOKEN from
   an AWS account like `Development` or `Shared-Services`.
   See https://d-90679fc682.awsapps.com/start/#/?tab=accounts
1. Run the `run-core-locally.sh` script, found in this repository's `bin` directory. That script
   accepts one optional argument: the location of appliction-config on your local disk. If that argument
   is omitted, the script looks for application-config in the same parent directory as this repository.
